# README #

README comportant toutes les informations relatives au projet.

# LICENSE #

This file is part of Mihawk - WoW-Emu.

WE-Editor is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

WE-Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with WE-Editor.  If not, see <http://www.gnu.org/licenses/>.

### Introduction - Qu'est-ce que WE-Editor ? ###

WE-Editor (Nom très original, je vous l'accorde, mais j'ai pas été inspiré.. xD) est un logiciel que j'ai créé pour compenser
le manque de mises à jour du côté de Truice, car il ne suivait plus les updates de Trinity, et ayant recompilé un core à la dernière
version, j'ai fais face à de nombreux problèmes... Donc, j'ai décidé de créer un nouvel éditeur, que je tâcherai de mettre à jour avec
les personnes voulant participer à ce projet ! :+1:

### Information sur WE-Editor ###

* Créateur : Mihawk
* Version : 2.2.0.0
* Langage utilisé : C#
* Librairie : http://puu.sh/lMMXo/75f72472e0.rar
* Screenshots : http://imgur.com/a/j5y4T

### Participants au projet ! ###

LeorFinacre, Nelidon, Belitharian

si vous voulez y participer, envoyez moi un message privé sur wow-emu :)
http://wow-emu.fr/private.php?action=send&uid=3