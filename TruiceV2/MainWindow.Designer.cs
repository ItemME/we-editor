﻿using System.IO;
using System;

/// This file is part of Mihawk - WoW-Emu.

/// WE-Editor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.

/// WE-Editor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.

/// You should have received a copy of the GNU General Public License
/// along with WE-Editor.  If not, see <http://www.gnu.org/licenses/>.

namespace TruiceV2
{
    partial class MainWindow
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.documentationDeTrinityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentationDeTrinityWoWEmuFrançaisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.materialTabSelector2 = new MaterialSkin.Controls.MaterialTabSelector();
            this.materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button10 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.textBox36 = new System.Windows.Forms.ComboBox();
            this.textBox34 = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBox33 = new System.Windows.Forms.ComboBox();
            this.textBox38 = new System.Windows.Forms.ComboBox();
            this.textBox70 = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label80 = new System.Windows.Forms.Label();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label61 = new System.Windows.Forms.Label();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox29 = new System.Windows.Forms.ComboBox();
            this.textBox24 = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox23 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label70 = new System.Windows.Forms.Label();
            this.textBox103 = new System.Windows.Forms.TextBox();
            this.textBox102 = new System.Windows.Forms.TextBox();
            this.textBox101 = new System.Windows.Forms.TextBox();
            this.textBox100 = new System.Windows.Forms.TextBox();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.textBox98 = new System.Windows.Forms.TextBox();
            this.textBox97 = new System.Windows.Forms.TextBox();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.textBox95 = new System.Windows.Forms.ComboBox();
            this.textBox89 = new System.Windows.Forms.ComboBox();
            this.textBox88 = new System.Windows.Forms.ComboBox();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.button11 = new System.Windows.Forms.Button();
            this.textBox93 = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.textBox96 = new System.Windows.Forms.TextBox();
            this.textBox94 = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.menuStrip1.SuspendLayout();
            this.materialTabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.textBox19.SuspendLayout();
            this.textBox30.SuspendLayout();
            this.textBox80.SuspendLayout();
            this.textBox39.SuspendLayout();
            this.textBox32.SuspendLayout();
            this.textBox31.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.textBox79.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.textBox91.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.documentationDeTrinityToolStripMenuItem,
            this.documentationDeTrinityWoWEmuFrançaisToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(484, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // documentationDeTrinityToolStripMenuItem
            // 
            this.documentationDeTrinityToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.documentationDeTrinityToolStripMenuItem.Name = "documentationDeTrinityToolStripMenuItem";
            this.documentationDeTrinityToolStripMenuItem.Size = new System.Drawing.Size(204, 20);
            this.documentationDeTrinityToolStripMenuItem.Text = "Documentation de Trinity (Anglais)";
            this.documentationDeTrinityToolStripMenuItem.Click += new System.EventHandler(this.documentationDeTrinityToolStripMenuItem_Click);
            // 
            // documentationDeTrinityWoWEmuFrançaisToolStripMenuItem
            // 
            this.documentationDeTrinityWoWEmuFrançaisToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.documentationDeTrinityWoWEmuFrançaisToolStripMenuItem.Name = "documentationDeTrinityWoWEmuFrançaisToolStripMenuItem";
            this.documentationDeTrinityWoWEmuFrançaisToolStripMenuItem.Size = new System.Drawing.Size(272, 20);
            this.documentationDeTrinityWoWEmuFrançaisToolStripMenuItem.Text = "Documentation de Trinity (WoWEmu - Français)";
            this.documentationDeTrinityWoWEmuFrançaisToolStripMenuItem.Click += new System.EventHandler(this.documentationDeTrinityWoWEmuFrançaisToolStripMenuItem_Click);
            // 
            // textBox83
            // 
            this.textBox83.Location = new System.Drawing.Point(12, 554);
            this.textBox83.Multiline = true;
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new System.Drawing.Size(1326, 138);
            this.textBox83.TabIndex = 166;
            // 
            // button2
            // 
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Location = new System.Drawing.Point(157, 525);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(122, 23);
            this.button2.TabIndex = 97;
            this.button2.Text = "Injecter le SQL";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(12, 525);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(122, 23);
            this.button1.TabIndex = 96;
            this.button1.Text = "Générer le SQL";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Location = new System.Drawing.Point(1256, 530);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(80, 13);
            this.linkLabel1.TabIndex = 167;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "© WoW-Emu.fr";
            // 
            // materialTabSelector2
            // 
            this.materialTabSelector2.BaseTabControl = this.materialTabControl1;
            this.materialTabSelector2.Depth = 0;
            this.materialTabSelector2.Location = new System.Drawing.Point(472, 27);
            this.materialTabSelector2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabSelector2.Name = "materialTabSelector2";
            this.materialTabSelector2.Size = new System.Drawing.Size(348, 32);
            this.materialTabSelector2.TabIndex = 170;
            this.materialTabSelector2.Text = "materialTabSelector2";
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Controls.Add(this.tabPage3);
            this.materialTabControl1.Controls.Add(this.tabPage4);
            this.materialTabControl1.Depth = 0;
            this.materialTabControl1.Location = new System.Drawing.Point(12, 76);
            this.materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(1326, 443);
            this.materialTabControl1.TabIndex = 176;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button10);
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Controls.Add(this.linkLabel4);
            this.tabPage3.Controls.Add(this.textBox82);
            this.tabPage3.Controls.Add(this.textBox57);
            this.tabPage3.Controls.Add(this.label82);
            this.tabPage3.Controls.Add(this.label57);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1318, 417);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Créature (Page 1)";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(844, 372);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(154, 25);
            this.button10.TabIndex = 189;
            this.button10.Text = "Recherche de sort";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.textBox36);
            this.groupBox8.Controls.Add(this.textBox34);
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this.label35);
            this.groupBox8.Controls.Add(this.label36);
            this.groupBox8.Controls.Add(this.label37);
            this.groupBox8.Controls.Add(this.textBox35);
            this.groupBox8.Controls.Add(this.textBox37);
            this.groupBox8.Location = new System.Drawing.Point(1129, 112);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(187, 204);
            this.groupBox8.TabIndex = 188;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Maître de compétences";
            // 
            // textBox36
            // 
            this.textBox36.FormattingEnabled = true;
            this.textBox36.Location = new System.Drawing.Point(6, 126);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(145, 21);
            this.textBox36.TabIndex = 175;
            // 
            // textBox34
            // 
            this.textBox34.FormattingEnabled = true;
            this.textBox34.Location = new System.Drawing.Point(6, 37);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(145, 21);
            this.textBox34.TabIndex = 175;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 22);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(62, 13);
            this.label34.TabIndex = 33;
            this.label34.Text = "trainer_type";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(7, 66);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(63, 13);
            this.label35.TabIndex = 34;
            this.label35.Text = "trainer_spell";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 110);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(66, 13);
            this.label36.TabIndex = 35;
            this.label36.Text = "trainer_class";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(7, 154);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(63, 13);
            this.label37.TabIndex = 36;
            this.label37.Text = "trainer_race";
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(6, 81);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(145, 20);
            this.textBox35.TabIndex = 119;
            this.textBox35.Text = "0";
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(6, 170);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(145, 20);
            this.textBox37.TabIndex = 120;
            this.textBox37.Text = "0";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.textBox33);
            this.groupBox7.Controls.Add(this.textBox38);
            this.groupBox7.Controls.Add(this.textBox70);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.textBox19);
            this.groupBox7.Controls.Add(this.label30);
            this.groupBox7.Controls.Add(this.textBox30);
            this.groupBox7.Controls.Add(this.label80);
            this.groupBox7.Controls.Add(this.textBox80);
            this.groupBox7.Controls.Add(this.label38);
            this.groupBox7.Controls.Add(this.label39);
            this.groupBox7.Controls.Add(this.textBox39);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Controls.Add(this.textBox32);
            this.groupBox7.Controls.Add(this.label31);
            this.groupBox7.Controls.Add(this.textBox31);
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Location = new System.Drawing.Point(807, 112);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(316, 248);
            this.groupBox7.TabIndex = 187;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Drapeaux";
            // 
            // textBox33
            // 
            this.textBox33.FormattingEnabled = true;
            this.textBox33.Location = new System.Drawing.Point(170, 169);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(133, 21);
            this.textBox33.TabIndex = 175;
            // 
            // textBox38
            // 
            this.textBox38.FormattingEnabled = true;
            this.textBox38.Location = new System.Drawing.Point(170, 81);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(133, 21);
            this.textBox38.TabIndex = 165;
            // 
            // textBox70
            // 
            this.textBox70.AutoSize = true;
            this.textBox70.Location = new System.Drawing.Point(6, 212);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(89, 17);
            this.textBox70.TabIndex = 164;
            this.textBox70.Text = "RacialLeader";
            this.textBox70.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(14, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "npcflag";
            // 
            // textBox19
            // 
            this.textBox19.Controls.Add(this.button4);
            this.textBox19.Location = new System.Drawing.Point(14, 38);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(133, 20);
            this.textBox19.TabIndex = 102;
            this.textBox19.Text = "0";
            // 
            // button4
            // 
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(108, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(20, 15);
            this.button4.TabIndex = 164;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(167, 22);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(52, 13);
            this.label30.TabIndex = 29;
            this.label30.Text = "unit_flags";
            // 
            // textBox30
            // 
            this.textBox30.Controls.Add(this.button5);
            this.textBox30.Location = new System.Drawing.Point(170, 37);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(133, 20);
            this.textBox30.TabIndex = 113;
            this.textBox30.Text = "0";
            // 
            // button5
            // 
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.Location = new System.Drawing.Point(108, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(20, 15);
            this.button5.TabIndex = 165;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(14, 66);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(58, 13);
            this.label80.TabIndex = 79;
            this.label80.Text = "flags_extra";
            // 
            // textBox80
            // 
            this.textBox80.Controls.Add(this.button6);
            this.textBox80.Location = new System.Drawing.Point(14, 82);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(133, 20);
            this.textBox80.TabIndex = 163;
            this.textBox80.Text = "0";
            // 
            // button6
            // 
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.Location = new System.Drawing.Point(108, 0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(20, 15);
            this.button6.TabIndex = 166;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(167, 65);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(27, 13);
            this.label38.TabIndex = 37;
            this.label38.Text = "type";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(14, 110);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(55, 13);
            this.label39.TabIndex = 38;
            this.label39.Text = "type_flags";
            // 
            // textBox39
            // 
            this.textBox39.Controls.Add(this.button7);
            this.textBox39.Location = new System.Drawing.Point(14, 126);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(133, 20);
            this.textBox39.TabIndex = 122;
            this.textBox39.Text = "0";
            // 
            // button7
            // 
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.Location = new System.Drawing.Point(108, 0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(20, 15);
            this.button7.TabIndex = 167;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(167, 110);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(68, 13);
            this.label32.TabIndex = 31;
            this.label32.Text = "dynamicflags";
            // 
            // textBox32
            // 
            this.textBox32.Controls.Add(this.button9);
            this.textBox32.Location = new System.Drawing.Point(170, 126);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(133, 20);
            this.textBox32.TabIndex = 115;
            this.textBox32.Text = "8";
            // 
            // button9
            // 
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.Image = ((System.Drawing.Image)(resources.GetObject("button9.Image")));
            this.button9.Location = new System.Drawing.Point(108, 0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(20, 15);
            this.button9.TabIndex = 169;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(14, 154);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(58, 13);
            this.label31.TabIndex = 30;
            this.label31.Text = "unit_flags2";
            // 
            // textBox31
            // 
            this.textBox31.Controls.Add(this.button8);
            this.textBox31.Location = new System.Drawing.Point(14, 170);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(133, 20);
            this.textBox31.TabIndex = 114;
            this.textBox31.Text = "0";
            // 
            // button8
            // 
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.Location = new System.Drawing.Point(108, 0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(20, 15);
            this.button8.TabIndex = 168;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(167, 156);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(33, 13);
            this.label33.TabIndex = 32;
            this.label33.Text = "family";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label61);
            this.groupBox5.Controls.Add(this.textBox61);
            this.groupBox5.Controls.Add(this.label62);
            this.groupBox5.Controls.Add(this.textBox62);
            this.groupBox5.Controls.Add(this.label63);
            this.groupBox5.Controls.Add(this.textBox63);
            this.groupBox5.Controls.Add(this.label64);
            this.groupBox5.Controls.Add(this.textBox81);
            this.groupBox5.Controls.Add(this.textBox77);
            this.groupBox5.Controls.Add(this.textBox64);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.textBox14);
            this.groupBox5.Controls.Add(this.label81);
            this.groupBox5.Controls.Add(this.label77);
            this.groupBox5.Location = new System.Drawing.Point(674, 112);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(127, 298);
            this.groupBox5.TabIndex = 185;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Comportement";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(6, 23);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(45, 13);
            this.label61.TabIndex = 60;
            this.label61.Text = "AIName";
            // 
            // textBox61
            // 
            this.textBox61.Location = new System.Drawing.Point(6, 40);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(45, 20);
            this.textBox61.TabIndex = 144;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(4, 68);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(81, 13);
            this.label62.TabIndex = 61;
            this.label62.Text = "MovementType";
            // 
            // textBox62
            // 
            this.textBox62.Location = new System.Drawing.Point(6, 84);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(115, 20);
            this.textBox62.TabIndex = 145;
            this.textBox62.Text = "0";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(6, 112);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(63, 13);
            this.label63.TabIndex = 62;
            this.label63.Text = "InhabitType";
            // 
            // textBox63
            // 
            this.textBox63.Location = new System.Drawing.Point(6, 128);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(115, 20);
            this.textBox63.TabIndex = 146;
            this.textBox63.Text = "1";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 156);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(67, 13);
            this.label64.TabIndex = 63;
            this.label64.Text = "HoverHeight";
            // 
            // textBox81
            // 
            this.textBox81.Location = new System.Drawing.Point(6, 260);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(115, 20);
            this.textBox81.TabIndex = 164;
            // 
            // textBox77
            // 
            this.textBox77.Location = new System.Drawing.Point(57, 39);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(64, 20);
            this.textBox77.TabIndex = 160;
            this.textBox77.Text = "0";
            // 
            // textBox64
            // 
            this.textBox64.Location = new System.Drawing.Point(6, 172);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(115, 20);
            this.textBox64.TabIndex = 147;
            this.textBox64.Text = "1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 200);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "gossip_menu_id";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(6, 216);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(115, 20);
            this.textBox14.TabIndex = 95;
            this.textBox14.Text = "0";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(6, 244);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(62, 13);
            this.label81.TabIndex = 80;
            this.label81.Text = "ScriptName";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(56, 22);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(65, 13);
            this.label77.TabIndex = 76;
            this.label77.Text = "movementId";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.textBox43);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.textBox44);
            this.groupBox4.Controls.Add(this.textBox45);
            this.groupBox4.Controls.Add(this.label46);
            this.groupBox4.Controls.Add(this.label47);
            this.groupBox4.Controls.Add(this.label48);
            this.groupBox4.Controls.Add(this.textBox46);
            this.groupBox4.Controls.Add(this.textBox47);
            this.groupBox4.Controls.Add(this.textBox48);
            this.groupBox4.Location = new System.Drawing.Point(553, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(209, 100);
            this.groupBox4.TabIndex = 184;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Résistances";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(4, 16);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(61, 13);
            this.label43.TabIndex = 42;
            this.label43.Text = "resistance1";
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(6, 31);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(60, 20);
            this.textBox43.TabIndex = 126;
            this.textBox43.Text = "0";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(72, 18);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(61, 13);
            this.label44.TabIndex = 43;
            this.label44.Text = "resistance2";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(142, 16);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(61, 13);
            this.label45.TabIndex = 44;
            this.label45.Text = "resistance3";
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(73, 31);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(60, 20);
            this.textBox44.TabIndex = 127;
            this.textBox44.Text = "0";
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(143, 31);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(60, 20);
            this.textBox45.TabIndex = 128;
            this.textBox45.Text = "0";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(5, 61);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(61, 13);
            this.label46.TabIndex = 45;
            this.label46.Text = "resistance4";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(74, 61);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(61, 13);
            this.label47.TabIndex = 46;
            this.label47.Text = "resistance5";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(141, 61);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(61, 13);
            this.label48.TabIndex = 47;
            this.label48.Text = "resistance6";
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(6, 76);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(60, 20);
            this.textBox46.TabIndex = 129;
            this.textBox46.Text = "0";
            // 
            // textBox47
            // 
            this.textBox47.Location = new System.Drawing.Point(75, 75);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(60, 20);
            this.textBox47.TabIndex = 130;
            this.textBox47.Text = "0";
            // 
            // textBox48
            // 
            this.textBox48.Location = new System.Drawing.Point(143, 74);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(60, 20);
            this.textBox48.TabIndex = 131;
            this.textBox48.Text = "0";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox29);
            this.groupBox3.Controls.Add(this.textBox24);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.textBox27);
            this.groupBox3.Controls.Add(this.textBox28);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.textBox25);
            this.groupBox3.Controls.Add(this.textBox79);
            this.groupBox3.Controls.Add(this.textBox26);
            this.groupBox3.Controls.Add(this.textBox78);
            this.groupBox3.Controls.Add(this.label68);
            this.groupBox3.Controls.Add(this.textBox68);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label49);
            this.groupBox3.Controls.Add(this.textBox49);
            this.groupBox3.Controls.Add(this.label50);
            this.groupBox3.Controls.Add(this.textBox50);
            this.groupBox3.Controls.Add(this.textBox51);
            this.groupBox3.Controls.Add(this.label51);
            this.groupBox3.Controls.Add(this.label52);
            this.groupBox3.Controls.Add(this.textBox52);
            this.groupBox3.Controls.Add(this.label53);
            this.groupBox3.Controls.Add(this.textBox53);
            this.groupBox3.Controls.Add(this.label54);
            this.groupBox3.Controls.Add(this.textBox56);
            this.groupBox3.Controls.Add(this.textBox54);
            this.groupBox3.Controls.Add(this.textBox55);
            this.groupBox3.Controls.Add(this.label55);
            this.groupBox3.Controls.Add(this.label56);
            this.groupBox3.Controls.Add(this.label78);
            this.groupBox3.Controls.Add(this.label79);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Location = new System.Drawing.Point(367, 112);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(298, 298);
            this.groupBox3.TabIndex = 183;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informations de combat";
            // 
            // textBox29
            // 
            this.textBox29.FormattingEnabled = true;
            this.textBox29.Location = new System.Drawing.Point(174, 254);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(116, 21);
            this.textBox29.TabIndex = 164;
            // 
            // textBox24
            // 
            this.textBox24.FormattingEnabled = true;
            this.textBox24.Location = new System.Drawing.Point(207, 81);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(83, 21);
            this.textBox24.TabIndex = 163;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(73, 13);
            this.label27.TabIndex = 26;
            this.label27.Text = "BaseVariance";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(108, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(81, 13);
            this.label28.TabIndex = 27;
            this.label28.Text = "RangeVariance";
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(9, 40);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(83, 20);
            this.textBox27.TabIndex = 110;
            this.textBox27.Text = "0";
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(111, 38);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(83, 20);
            this.textBox28.TabIndex = 111;
            this.textBox28.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 66);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(85, 13);
            this.label25.TabIndex = 24;
            this.label25.Text = "BaseAttackTime";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(108, 66);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(93, 13);
            this.label26.TabIndex = 25;
            this.label26.Text = "RangeAttackTime";
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(9, 81);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(83, 20);
            this.textBox25.TabIndex = 108;
            this.textBox25.Text = "0";
            // 
            // textBox79
            // 
            this.textBox79.Controls.Add(this.button3);
            this.textBox79.Location = new System.Drawing.Point(129, 122);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(123, 20);
            this.textBox79.TabIndex = 162;
            this.textBox79.Text = "0";
            // 
            // button3
            // 
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(98, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(20, 15);
            this.button3.TabIndex = 163;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(111, 81);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(83, 20);
            this.textBox26.TabIndex = 109;
            this.textBox26.Text = "0";
            // 
            // textBox78
            // 
            this.textBox78.Location = new System.Drawing.Point(207, 37);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(83, 20);
            this.textBox78.TabIndex = 161;
            this.textBox78.Text = "1";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(8, 106);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(84, 13);
            this.label68.TabIndex = 67;
            this.label68.Text = "DamageModifier";
            // 
            // textBox68
            // 
            this.textBox68.Location = new System.Drawing.Point(9, 122);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(83, 20);
            this.textBox68.TabIndex = 151;
            this.textBox68.Text = "1";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(207, 66);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(58, 13);
            this.label24.TabIndex = 23;
            this.label24.Text = "dmgschool";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(8, 150);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(34, 13);
            this.label49.TabIndex = 48;
            this.label49.Text = "spell1";
            // 
            // textBox49
            // 
            this.textBox49.Location = new System.Drawing.Point(9, 166);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(60, 20);
            this.textBox49.TabIndex = 132;
            this.textBox49.Text = "0";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(88, 150);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(34, 13);
            this.label50.TabIndex = 49;
            this.label50.Text = "spell2";
            // 
            // textBox50
            // 
            this.textBox50.Location = new System.Drawing.Point(91, 166);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(60, 20);
            this.textBox50.TabIndex = 133;
            this.textBox50.Text = "0";
            // 
            // textBox51
            // 
            this.textBox51.Location = new System.Drawing.Point(174, 166);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(60, 20);
            this.textBox51.TabIndex = 134;
            this.textBox51.Text = "0";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(173, 150);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(34, 13);
            this.label51.TabIndex = 50;
            this.label51.Text = "spell3";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(7, 193);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 13);
            this.label52.TabIndex = 51;
            this.label52.Text = "spell4";
            // 
            // textBox52
            // 
            this.textBox52.Location = new System.Drawing.Point(10, 209);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(60, 20);
            this.textBox52.TabIndex = 135;
            this.textBox52.Text = "0";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(88, 193);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 52;
            this.label53.Text = "spell5";
            // 
            // textBox53
            // 
            this.textBox53.Location = new System.Drawing.Point(91, 209);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(60, 20);
            this.textBox53.TabIndex = 136;
            this.textBox53.Text = "0";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(171, 194);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(34, 13);
            this.label54.TabIndex = 53;
            this.label54.Text = "spell6";
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(91, 254);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(60, 20);
            this.textBox56.TabIndex = 139;
            this.textBox56.Text = "0";
            // 
            // textBox54
            // 
            this.textBox54.Location = new System.Drawing.Point(174, 210);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(60, 20);
            this.textBox54.TabIndex = 137;
            this.textBox54.Text = "0";
            // 
            // textBox55
            // 
            this.textBox55.Location = new System.Drawing.Point(6, 254);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(60, 20);
            this.textBox55.TabIndex = 138;
            this.textBox55.Text = "0";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(7, 238);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(34, 13);
            this.label55.TabIndex = 54;
            this.label55.Text = "spell7";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(88, 238);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(34, 13);
            this.label56.TabIndex = 55;
            this.label56.Text = "spell8";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(204, 22);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(70, 13);
            this.label78.TabIndex = 77;
            this.label78.Text = "RegenHealth";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(126, 107);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(126, 13);
            this.label79.TabIndex = 78;
            this.label79.Text = "mechanic_immune_mask";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(173, 236);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 13);
            this.label29.TabIndex = 28;
            this.label29.Text = "unit_class";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this.textBox40);
            this.groupBox2.Controls.Add(this.textBox41);
            this.groupBox2.Controls.Add(this.textBox42);
            this.groupBox2.Location = new System.Drawing.Point(367, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(180, 100);
            this.groupBox2.TabIndex = 182;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Butin";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 28);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(32, 13);
            this.label40.TabIndex = 39;
            this.label40.Text = "lootid";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 53);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(68, 13);
            this.label41.TabIndex = 40;
            this.label41.Text = "pitpocketloot";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 77);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(43, 13);
            this.label42.TabIndex = 41;
            this.label42.Text = "skinloot";
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(80, 25);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(94, 20);
            this.textBox40.TabIndex = 123;
            this.textBox40.Text = "0";
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(80, 48);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(94, 20);
            this.textBox41.TabIndex = 124;
            this.textBox41.Text = "0";
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(80, 73);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(94, 20);
            this.textBox42.TabIndex = 125;
            this.textBox42.Text = "0";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox23);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox69);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBox11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.textBox12);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.textBox67);
            this.groupBox1.Controls.Add(this.textBox13);
            this.groupBox1.Controls.Add(this.textBox66);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBox65);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label69);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.textBox58);
            this.groupBox1.Controls.Add(this.textBox60);
            this.groupBox1.Controls.Add(this.textBox9);
            this.groupBox1.Controls.Add(this.textBox59);
            this.groupBox1.Controls.Add(this.textBox10);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.textBox17);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.textBox15);
            this.groupBox1.Controls.Add(this.textBox16);
            this.groupBox1.Controls.Add(this.label59);
            this.groupBox1.Controls.Add(this.label60);
            this.groupBox1.Controls.Add(this.label58);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.textBox18);
            this.groupBox1.Controls.Add(this.label65);
            this.groupBox1.Controls.Add(this.label66);
            this.groupBox1.Controls.Add(this.label67);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.textBox20);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.textBox21);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.textBox22);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 404);
            this.groupBox1.TabIndex = 181;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informations générales sur le PNJ";
            // 
            // textBox23
            // 
            this.textBox23.AccessibleDescription = "";
            this.textBox23.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox23.FormattingEnabled = true;
            this.textBox23.Location = new System.Drawing.Point(190, 359);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(159, 21);
            this.textBox23.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(11, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(56, 20);
            this.textBox1.TabIndex = 82;
            this.textBox1.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "entry";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(73, 39);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(92, 20);
            this.textBox2.TabIndex = 83;
            this.textBox2.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "difficulty_entry_1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(171, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "difficulty_entry_2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(263, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "difficulty_entry_3";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(171, 39);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(86, 20);
            this.textBox3.TabIndex = 84;
            this.textBox3.Text = "0";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(263, 38);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(86, 20);
            this.textBox4.TabIndex = 85;
            this.textBox4.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "KillCredit1";
            // 
            // textBox69
            // 
            this.textBox69.Location = new System.Drawing.Point(255, 174);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(94, 20);
            this.textBox69.TabIndex = 152;
            this.textBox69.Text = "1";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(11, 85);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(154, 20);
            this.textBox5.TabIndex = 86;
            this.textBox5.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(171, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "KillCredit2";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(174, 83);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(175, 20);
            this.textBox6.TabIndex = 87;
            this.textBox6.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "name";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(11, 129);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(338, 20);
            this.textBox11.TabIndex = 92;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 158);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "subname";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(11, 173);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(154, 20);
            this.textBox12.TabIndex = 93;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(171, 158);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "IconName";
            // 
            // textBox67
            // 
            this.textBox67.Location = new System.Drawing.Point(262, 312);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(87, 20);
            this.textBox67.TabIndex = 150;
            this.textBox67.Text = "1";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(174, 173);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(68, 20);
            this.textBox13.TabIndex = 94;
            // 
            // textBox66
            // 
            this.textBox66.Location = new System.Drawing.Point(172, 312);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(87, 20);
            this.textBox66.TabIndex = 149;
            this.textBox66.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "modelid1";
            // 
            // textBox65
            // 
            this.textBox65.Location = new System.Drawing.Point(82, 312);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(87, 20);
            this.textBox65.TabIndex = 148;
            this.textBox65.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(82, 207);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "modelid2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(152, 207);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "modelid3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(225, 207);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "modelid4";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(252, 159);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(97, 13);
            this.label69.TabIndex = 68;
            this.label69.Text = "ExperienceModifier";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(11, 222);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(67, 20);
            this.textBox7.TabIndex = 88;
            this.textBox7.Text = "0";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(82, 222);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(67, 20);
            this.textBox8.TabIndex = 89;
            this.textBox8.Text = "0";
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(301, 266);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(48, 20);
            this.textBox58.TabIndex = 141;
            this.textBox58.Text = "0";
            // 
            // textBox60
            // 
            this.textBox60.Location = new System.Drawing.Point(227, 266);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(67, 20);
            this.textBox60.TabIndex = 143;
            this.textBox60.Text = "0";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(155, 222);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(67, 20);
            this.textBox9.TabIndex = 90;
            this.textBox9.Text = "0";
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(155, 266);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(67, 20);
            this.textBox59.TabIndex = 142;
            this.textBox59.Text = "0";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(228, 222);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(67, 20);
            this.textBox10.TabIndex = 91;
            this.textBox10.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(300, 207);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(24, 13);
            this.label17.TabIndex = 16;
            this.label17.Text = "exp";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(301, 222);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(48, 20);
            this.textBox17.TabIndex = 100;
            this.textBox17.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 251);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "minlevel";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(82, 251);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "maxlevel";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(11, 266);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(67, 20);
            this.textBox15.TabIndex = 98;
            this.textBox15.Text = "1";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(82, 266);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(67, 20);
            this.textBox16.TabIndex = 99;
            this.textBox16.Text = "1";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(152, 251);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(43, 13);
            this.label59.TabIndex = 58;
            this.label59.Text = "mingold";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(224, 251);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(46, 13);
            this.label60.TabIndex = 59;
            this.label60.Text = "maxgold";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(300, 251);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(51, 13);
            this.label58.TabIndex = 57;
            this.label58.Text = "VehicleId";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(10, 297);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "faction";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(11, 312);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(65, 20);
            this.textBox18.TabIndex = 101;
            this.textBox18.Text = "0";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(79, 297);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(75, 13);
            this.label65.TabIndex = 64;
            this.label65.Text = "HeatlhModifier";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(171, 297);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(71, 13);
            this.label66.TabIndex = 65;
            this.label66.Text = "ManaModifier";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(259, 297);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(71, 13);
            this.label67.TabIndex = 66;
            this.label67.Text = "ArmorModifier";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(8, 344);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 13);
            this.label20.TabIndex = 19;
            this.label20.Text = "speed_walk";
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(9, 359);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(63, 20);
            this.textBox20.TabIndex = 103;
            this.textBox20.Text = "1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(75, 345);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(57, 13);
            this.label21.TabIndex = 20;
            this.label21.Text = "speed_run";
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(78, 360);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(63, 20);
            this.textBox21.TabIndex = 104;
            this.textBox21.Text = "1.113";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(143, 344);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 13);
            this.label22.TabIndex = 21;
            this.label22.Text = "scale";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(145, 359);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(40, 20);
            this.textBox22.TabIndex = 105;
            this.textBox22.Text = "1";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(187, 344);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(28, 13);
            this.label23.TabIndex = 22;
            this.label23.Text = "rank";
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.Location = new System.Drawing.Point(1178, 356);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(92, 13);
            this.linkLabel4.TabIndex = 180;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "creature_template";
            // 
            // textBox82
            // 
            this.textBox82.Location = new System.Drawing.Point(869, 60);
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new System.Drawing.Size(100, 20);
            this.textBox82.TabIndex = 179;
            this.textBox82.Text = "12340";
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(869, 34);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(100, 20);
            this.textBox57.TabIndex = 178;
            this.textBox57.Text = "0";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(785, 63);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(65, 13);
            this.label82.TabIndex = 177;
            this.label82.Text = "VerifiedBuild";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(785, 37);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(78, 13);
            this.label57.TabIndex = 176;
            this.label57.Text = "PetSpellDataId";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label70);
            this.tabPage4.Controls.Add(this.textBox103);
            this.tabPage4.Controls.Add(this.textBox102);
            this.tabPage4.Controls.Add(this.textBox101);
            this.tabPage4.Controls.Add(this.textBox100);
            this.tabPage4.Controls.Add(this.textBox99);
            this.tabPage4.Controls.Add(this.textBox98);
            this.tabPage4.Controls.Add(this.textBox97);
            this.tabPage4.Controls.Add(this.label101);
            this.tabPage4.Controls.Add(this.label100);
            this.tabPage4.Controls.Add(this.label99);
            this.tabPage4.Controls.Add(this.label98);
            this.tabPage4.Controls.Add(this.label97);
            this.tabPage4.Controls.Add(this.label96);
            this.tabPage4.Controls.Add(this.linkLabel3);
            this.tabPage4.Controls.Add(this.groupBox10);
            this.tabPage4.Controls.Add(this.groupBox9);
            this.tabPage4.Controls.Add(this.textBox96);
            this.tabPage4.Controls.Add(this.textBox94);
            this.tabPage4.Controls.Add(this.label95);
            this.tabPage4.Controls.Add(this.label93);
            this.tabPage4.Controls.Add(this.linkLabel2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1318, 417);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Créature (Page 2)";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(655, 10);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(9, 403);
            this.label70.TabIndex = 50;
            this.label70.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n" +
    "|\r\n|\r\n|\r\n|\r\n";
            // 
            // textBox103
            // 
            this.textBox103.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox103.ForeColor = System.Drawing.Color.Red;
            this.textBox103.Location = new System.Drawing.Point(682, 232);
            this.textBox103.Multiline = true;
            this.textBox103.Name = "textBox103";
            this.textBox103.ReadOnly = true;
            this.textBox103.ShortcutsEnabled = false;
            this.textBox103.Size = new System.Drawing.Size(619, 45);
            this.textBox103.TabIndex = 49;
            this.textBox103.Text = "INFORMATION : Si vous ne rentrez rien dans la case \"entry\" d\'une des deux parties" +
    ", celle-ci sera automatiquement ignorée par la génération et l\'injection du SQL " +
    "!\r\n";
            this.textBox103.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox103.UseWaitCursor = true;
            // 
            // textBox102
            // 
            this.textBox102.Location = new System.Drawing.Point(682, 125);
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new System.Drawing.Size(183, 20);
            this.textBox102.TabIndex = 48;
            this.textBox102.Text = "12340";
            // 
            // textBox101
            // 
            this.textBox101.Location = new System.Drawing.Point(987, 79);
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new System.Drawing.Size(192, 20);
            this.textBox101.TabIndex = 47;
            this.textBox101.Text = "0";
            // 
            // textBox100
            // 
            this.textBox100.Location = new System.Drawing.Point(987, 53);
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new System.Drawing.Size(192, 20);
            this.textBox100.TabIndex = 46;
            this.textBox100.Text = "0";
            // 
            // textBox99
            // 
            this.textBox99.Location = new System.Drawing.Point(987, 24);
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new System.Drawing.Size(192, 20);
            this.textBox99.TabIndex = 45;
            this.textBox99.Text = "0";
            // 
            // textBox98
            // 
            this.textBox98.Location = new System.Drawing.Point(682, 76);
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new System.Drawing.Size(218, 20);
            this.textBox98.TabIndex = 44;
            this.textBox98.Text = "1";
            // 
            // textBox97
            // 
            this.textBox97.Location = new System.Drawing.Point(682, 27);
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new System.Drawing.Size(218, 20);
            this.textBox97.TabIndex = 43;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(679, 109);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(65, 13);
            this.label101.TabIndex = 42;
            this.label101.Text = "VerifiedBuild";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(925, 82);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(56, 13);
            this.label100.TabIndex = 41;
            this.label100.Text = "itemEntry3";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(925, 56);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(56, 13);
            this.label99.TabIndex = 40;
            this.label99.Text = "itemEntry2";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(925, 27);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(56, 13);
            this.label98.TabIndex = 39;
            this.label98.Text = "itemEntry1";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(679, 60);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(15, 13);
            this.label97.TabIndex = 38;
            this.label97.Text = "id";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(679, 11);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(30, 13);
            this.label96.TabIndex = 37;
            this.label96.Text = "entry";
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(1190, 379);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(124, 13);
            this.linkLabel3.TabIndex = 36;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "creature_equip_template";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.textBox95);
            this.groupBox10.Controls.Add(this.textBox89);
            this.groupBox10.Controls.Add(this.textBox88);
            this.groupBox10.Controls.Add(this.textBox87);
            this.groupBox10.Controls.Add(this.label86);
            this.groupBox10.Controls.Add(this.label87);
            this.groupBox10.Controls.Add(this.textBox91);
            this.groupBox10.Controls.Add(this.textBox93);
            this.groupBox10.Controls.Add(this.label94);
            this.groupBox10.Controls.Add(this.label90);
            this.groupBox10.Controls.Add(this.textBox92);
            this.groupBox10.Controls.Add(this.label88);
            this.groupBox10.Controls.Add(this.label92);
            this.groupBox10.Controls.Add(this.textBox90);
            this.groupBox10.Controls.Add(this.label89);
            this.groupBox10.Controls.Add(this.label91);
            this.groupBox10.Location = new System.Drawing.Point(6, 149);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(626, 217);
            this.groupBox10.TabIndex = 35;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Texte";
            // 
            // textBox95
            // 
            this.textBox95.FormattingEnabled = true;
            this.textBox95.Location = new System.Drawing.Point(508, 44);
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new System.Drawing.Size(95, 21);
            this.textBox95.TabIndex = 28;
            // 
            // textBox89
            // 
            this.textBox89.FormattingEnabled = true;
            this.textBox89.Location = new System.Drawing.Point(300, 94);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(95, 21);
            this.textBox89.TabIndex = 27;
            // 
            // textBox88
            // 
            this.textBox88.FormattingEnabled = true;
            this.textBox88.Location = new System.Drawing.Point(300, 44);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(95, 21);
            this.textBox88.TabIndex = 26;
            // 
            // textBox87
            // 
            this.textBox87.AllowDrop = true;
            this.textBox87.Location = new System.Drawing.Point(16, 45);
            this.textBox87.Multiline = true;
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(240, 159);
            this.textBox87.TabIndex = 17;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(14, 29);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(24, 13);
            this.label86.TabIndex = 4;
            this.label86.Text = "text";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(297, 29);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(27, 13);
            this.label87.TabIndex = 5;
            this.label87.Text = "type";
            // 
            // textBox91
            // 
            this.textBox91.Controls.Add(this.button11);
            this.textBox91.Location = new System.Drawing.Point(404, 45);
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new System.Drawing.Size(95, 20);
            this.textBox91.TabIndex = 21;
            this.textBox91.Text = "0";
            // 
            // button11
            // 
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.Location = new System.Drawing.Point(70, 0);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(20, 15);
            this.button11.TabIndex = 164;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click_1);
            // 
            // textBox93
            // 
            this.textBox93.Location = new System.Drawing.Point(404, 143);
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new System.Drawing.Size(95, 20);
            this.textBox93.TabIndex = 23;
            this.textBox93.Text = "0";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(505, 29);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(60, 13);
            this.label94.TabIndex = 12;
            this.label94.Text = "TextRange";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(401, 31);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(36, 13);
            this.label90.TabIndex = 8;
            this.label90.Text = "emote";
            // 
            // textBox92
            // 
            this.textBox92.Location = new System.Drawing.Point(300, 143);
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new System.Drawing.Size(95, 20);
            this.textBox92.TabIndex = 22;
            this.textBox92.Text = "0";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(297, 78);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(51, 13);
            this.label88.TabIndex = 6;
            this.label88.Text = "language";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(401, 127);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(36, 13);
            this.label92.TabIndex = 10;
            this.label92.Text = "sound";
            // 
            // textBox90
            // 
            this.textBox90.Location = new System.Drawing.Point(404, 94);
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new System.Drawing.Size(95, 20);
            this.textBox90.TabIndex = 20;
            this.textBox90.Text = "100";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(401, 78);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(54, 13);
            this.label89.TabIndex = 7;
            this.label89.Text = "probability";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(297, 127);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(45, 13);
            this.label91.TabIndex = 9;
            this.label91.Text = "duration";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label85);
            this.groupBox9.Controls.Add(this.label83);
            this.groupBox9.Controls.Add(this.label84);
            this.groupBox9.Controls.Add(this.textBox85);
            this.groupBox9.Controls.Add(this.textBox84);
            this.groupBox9.Controls.Add(this.textBox86);
            this.groupBox9.Location = new System.Drawing.Point(6, 10);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(178, 112);
            this.groupBox9.TabIndex = 34;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Informations d\'indentification";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(13, 82);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(15, 13);
            this.label85.TabIndex = 3;
            this.label85.Text = "id";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(13, 26);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(30, 13);
            this.label83.TabIndex = 1;
            this.label83.Text = "entry";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(13, 55);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(42, 13);
            this.label84.TabIndex = 2;
            this.label84.Text = "groupid";
            // 
            // textBox85
            // 
            this.textBox85.Location = new System.Drawing.Point(66, 52);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(106, 20);
            this.textBox85.TabIndex = 15;
            this.textBox85.Text = "0";
            // 
            // textBox84
            // 
            this.textBox84.Location = new System.Drawing.Point(66, 23);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(106, 20);
            this.textBox84.TabIndex = 14;
            // 
            // textBox86
            // 
            this.textBox86.Location = new System.Drawing.Point(66, 79);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(106, 20);
            this.textBox86.TabIndex = 16;
            this.textBox86.Text = "0";
            // 
            // textBox96
            // 
            this.textBox96.Location = new System.Drawing.Point(306, 75);
            this.textBox96.Multiline = true;
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new System.Drawing.Size(253, 48);
            this.textBox96.TabIndex = 33;
            // 
            // textBox94
            // 
            this.textBox94.Location = new System.Drawing.Point(306, 26);
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new System.Drawing.Size(253, 20);
            this.textBox94.TabIndex = 32;
            this.textBox94.Text = "0";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(303, 59);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(50, 13);
            this.label95.TabIndex = 31;
            this.label95.Text = "comment";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(303, 10);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(85, 13);
            this.label93.TabIndex = 30;
            this.label93.Text = "BroadcastTextId";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(563, 379);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(69, 13);
            this.linkLabel2.TabIndex = 29;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "creature_text";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1355, 707);
            this.Controls.Add(this.materialTabControl1);
            this.Controls.Add(this.materialTabSelector2);
            this.Controls.Add(this.textBox83);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WE-Editor - Outil de création pour TrinityCore (LastRev.)";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.materialTabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.textBox19.ResumeLayout(false);
            this.textBox30.ResumeLayout(false);
            this.textBox80.ResumeLayout(false);
            this.textBox39.ResumeLayout(false);
            this.textBox32.ResumeLayout(false);
            this.textBox31.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.textBox79.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.textBox91.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem documentationDeTrinityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem documentationDeTrinityWoWEmuFrançaisToolStripMenuItem;
        private System.Windows.Forms.LinkLabel linkLabel1;

        private ReadCSV RankCSV = new ReadCSV("Rank.csv");
        private ReadCSV TrainerTypeCSV = new ReadCSV("TrainerType.csv");
        private ReadCSV ChrClassesCSV = new ReadCSV("ChrClasses.csv");
        private ReadCSV CreatureTypeCSV = new ReadCSV("CreatureType.csv");
        private ReadCSV CreatureFamilyCSV = new ReadCSV("CreatureFamily.csv");
        private ReadCSV TextTypeCSV = new ReadCSV("TextType.csv");
        public ReadCSV TextRangeCSV = new ReadCSV("TextRange.csv");
        public ReadCSV LanguagesCSV = new ReadCSV("Languages.csv");
        public ReadCSV dmgSchoolCSV = new ReadCSV("dmgSchool.csv");
        public ReadCSV UnitClassCSV = new ReadCSV("UnitClass.csv");

        public static ReadCSV ImmuneMaskCSV = new ReadCSV("CreatureImmuneMask.csv");
        public static ReadCSV NPCFlagCSV = new ReadCSV("CreatureNPCFlags.csv");
        public static ReadCSV UnitFlagCSV = new ReadCSV("CreatureFlags.csv");
        public static ReadCSV DynamicFlagCSV = new ReadCSV("CreatureDynamicFlags.csv");
        public static ReadCSV FlagExtraCSV = new ReadCSV("CreatureFlagsExtra.csv");
        public static ReadCSV TypeFlagCSV = new ReadCSV("CreatureTypeFlags.csv");
        public static ReadCSV UnitFlag2CSV = new ReadCSV("CreatureUnitFlags2.csv");
        public static ReadCSV SpellCSV = new ReadCSV("Spell.csv");
        public static ReadCSV TextEmoteCSV = new ReadCSV("TextEmote.csv");
        private MaterialSkin.Controls.MaterialTabSelector materialTabSelector2;
        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox textBox36;
        private System.Windows.Forms.ComboBox textBox34;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox textBox33;
        private System.Windows.Forms.ComboBox textBox38;
        private System.Windows.Forms.CheckBox textBox70;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox textBox29;
        private System.Windows.Forms.ComboBox textBox24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox textBox23;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox textBox103;
        private System.Windows.Forms.TextBox textBox102;
        private System.Windows.Forms.TextBox textBox101;
        private System.Windows.Forms.TextBox textBox100;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.TextBox textBox98;
        private System.Windows.Forms.TextBox textBox97;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox textBox95;
        private System.Windows.Forms.ComboBox textBox89;
        private System.Windows.Forms.ComboBox textBox88;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox textBox93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.TextBox textBox96;
        private System.Windows.Forms.TextBox textBox94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label label70;
    }
}