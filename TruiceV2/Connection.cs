﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.Diagnostics;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;

/// This file is part of Mihawk - WoW-Emu.

/// WE-Editor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.

/// WE-Editor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.

/// You should have received a copy of the GNU General Public License
/// along with WE-Editor.  If not, see <http://www.gnu.org/licenses/>.

namespace TruiceV2
{
    public partial class Connection : MaterialForm
    {
        public Connection()
        {
            InitializeComponent();
        }

        void fill_listbox()
        {
            // Définition des variables

            string host = textBox1.Text;
            string port = textBox5.Text;
            string user = textBox2.Text;
            string pass = textBox3.Text;
            string db = textBox4.Text;

            // Sauvegarde dans le XML

            XDocument xdoc = new XDocument(
                new XElement("MySQL",
                    new XAttribute("Hôte", textBox1.Text),
                    new XAttribute("Port", textBox5.Text),
                    new XAttribute("ID", textBox2.Text),
                    new XAttribute("MDP", textBox3.Text),
                    new XAttribute("DB", textBox4.Text)));

            xdoc.Save(AppDomain.CurrentDomain.BaseDirectory + "WE-Editor.xml");

            // Définition de la constante de connexion (pour le test)

            String constring = @"server=" + host + ";port=" + port + ";database=" + db + ";userid=" + user + ";password=" + pass + ";";
            MySqlConnection conDataBase = new MySqlConnection(constring);
            try
            {

                // Test de connexion

                conDataBase.Open();
                conDataBase.Close();
                MessageBox.Show("Sauvegarde effectuée, ouverture de l'éditeur", "Sauvegarde effectuée",MessageBoxButtons.OK, MessageBoxIcon.Information);

                // Fin du test, fermeture de Connection et ouverture de MainWindow

                MainWindow MainWindow = new MainWindow();
                MainWindow.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                // Si il y a une erreur

                MessageBox.Show(ex.Message);
            }
        }

        private void Connection_Load(object sender, EventArgs e)
        {
            // Détéction du premier démarrage

            if (System.IO.File.Exists("WE-Editor.xml"))
            {

            }

            else
            {
                // Création du XML de sauvegarde

                XDocument xdoc = new XDocument(
                    new XElement("MySQL",
                        new XAttribute("Hôte", "localhost"),
                        new XAttribute("Port", "3306"),
                        new XAttribute("ID", "root"),
                        new XAttribute("MDP", "ascent"),
                        new XAttribute("DB", "world")));

                xdoc.Save(AppDomain.CurrentDomain.BaseDirectory + "WE-Editor.xml");
                MessageBox.Show("Aucun fichier de sauvegarde trouvé, création de 'WE-Editor.xml' effectuée");
            }

            // Lecture du XML

            ArrayList host = XPathNav.SearchXPathNavigator("MySQL", "Hôte");
            ArrayList port = XPathNav.SearchXPathNavigator("MySQL", "Port");
            ArrayList user = XPathNav.SearchXPathNavigator("MySQL", "ID");
            ArrayList pass = XPathNav.SearchXPathNavigator("MySQL", "MDP");
            ArrayList db = XPathNav.SearchXPathNavigator("MySQL", "DB");

            // Conversion du ArrayList en String
            StringBuilder hostto = new StringBuilder();
            foreach (object obj in host) hostto.Append(obj);
            string newhost = hostto.ToString();
            textBox1.Text = newhost;

            // Conversion du ArrayList en String
            StringBuilder portto = new StringBuilder();
            foreach (object obj in port) portto.Append(obj);
            string newport = portto.ToString();
            textBox5.Text = newport;

            // Conversion du ArrayList en String
            StringBuilder userto = new StringBuilder();
            foreach (object obj in user) userto.Append(obj);
            string newuser = userto.ToString();
            textBox2.Text = newuser;

            // Conversion du ArrayList en String
            StringBuilder passto = new StringBuilder();
            foreach (object obj in pass) passto.Append(obj);
            string newpass = passto.ToString();
            textBox3.Text = newpass;

            // Conversion du ArrayList en String
            StringBuilder dbto = new StringBuilder();
            foreach (object obj in db) dbto.Append(obj);
            string newdb = dbto.ToString();
            textBox4.Text = newdb;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Fonction à effectuer, quand on appui sur le bouton de la Connection

            fill_listbox();
        }

    }

    // Définition de la Class pour les XML (ne pas toucher)

    class XPathNav
    {
        private static string XMLFilePath;

        static XPathNav()
        {
            XMLFilePath = @"WE-Editor.xml";
        }

        /// <summary>
        /// Méthode vierge permettant d'activer la classe statique
        /// </summary>

        public static void init() { }

        /// <summary>
        /// Récupère la valeur de l'attribut du noeud recherché dans le fichier de configuration
        /// </summary>
        /// <param name="xPathString">Expression XPath de recherche du noeud</param>
        /// <param name="attribute">Attribut à rechercher</param>
        /// <returns>Une ArrayList contenant la liste des attributs recherchés</returns>

        public static ArrayList SearchXPathNavigator(string xPathString, string attribute)
        {
            // Initilisation des variables
            XPathDocument xpathDoc;
            XPathNavigator xpathNavigator;
            XPathNodeIterator xpathNodeIterator;
            XPathExpression expr;
            ArrayList listOfAttributes = new ArrayList();

            // Parcours du fichier XML
            try
            {
                xpathDoc = new XPathDocument(XMLFilePath);
                xpathNavigator = xpathDoc.CreateNavigator();

                expr = xpathNavigator.Compile(xPathString);
                xpathNodeIterator = xpathNavigator.Select(expr);

                while (xpathNodeIterator.MoveNext())
                {
                    // On récupère l'attribut
                    listOfAttributes.Add(xpathNodeIterator.Current.GetAttribute(attribute, ""));
                }
            }
            catch (Exception e)
            {
            }
            return listOfAttributes;
        }
    }
}


