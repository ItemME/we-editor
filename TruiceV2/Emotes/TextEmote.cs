﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/// This file is part of Mihawk - WoW-Emu.

/// WE-Editor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.

/// WE-Editor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.

/// You should have received a copy of the GNU General Public License
/// along with WE-Editor.  If not, see <http://www.gnu.org/licenses/>.

namespace TruiceV2
{
    public partial class TextEmote : Form
    {
        public UInt64 m_uiEmoteID;

        public TextEmote()
        {
            InitializeComponent();
        }

        private void dynamicFlag_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < MainWindow.TextEmoteCSV.Length; i++)
                listBox1.Items.Insert(i, MainWindow.TextEmoteCSV.Name[i]);
        }

        private void dynamicFlag_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                for (int i = 0; i < listBox1.Items.Count; i++)
                {
                    m_uiEmoteID = MainWindow.TextEmoteCSV.Value[listBox1.SelectedIndex];
                }
            }
        }

        public UInt64 getEmoteID()
        {
            return m_uiEmoteID;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}