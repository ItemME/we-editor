﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.Diagnostics;
using System.Text.RegularExpressions;
using MaterialSkin;
using MaterialSkin.Animations;
using MaterialSkin.Controls;

/// This file is part of Mihawk - WoW-Emu.

/// WE-Editor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.

/// WE-Editor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.

/// You should have received a copy of the GNU General Public License
/// along with WE-Editor.  If not, see <http://www.gnu.org/licenses/>.

namespace TruiceV2
{
    public partial class MainWindow : MaterialForm
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            //////////////////////////////////////////////////////////////
            // LECTURE DES CSV
            // Rang d'honneur
            for (int i = 0; i < RankCSV.Length; i++)
                textBox23.Items.Add(RankCSV.Name[i]);
            textBox23.Text = RankCSV.Name[0];

            // Type d'entraîneur
            for (int i = 0; i < TrainerTypeCSV.Length; i++)
                textBox34.Items.Add(TrainerTypeCSV.Name[i]);
            textBox34.Text = TrainerTypeCSV.Name[0];

            // Classes
            for (int i = 0; i < ChrClassesCSV.Length; i++)
                textBox36.Items.Add(ChrClassesCSV.Name[i]);
            textBox36.Text = ChrClassesCSV.Name[0];

            // Type de créature
            for (int i = 0; i < CreatureTypeCSV.Length; i++)
                textBox38.Items.Add(CreatureTypeCSV.Name[i]);
            textBox38.Text = CreatureTypeCSV.Name[0];

            // Famille de créature
            for (int i = 0; i < CreatureFamilyCSV.Length; i++)
                textBox33.Items.Add(CreatureFamilyCSV.Name[i]);
            textBox33.Text = CreatureFamilyCSV.Name[0];

            // Types de texte
            for (int i = 0; i < TextTypeCSV.Length; i++)
                textBox88.Items.Add(TextTypeCSV.Name[i]);
            textBox88.Text = TextTypeCSV.Name[0];

            // Range de texte
            for (int i = 0; i < TextRangeCSV.Length; i++)
                textBox95.Items.Add(TextRangeCSV.Name[i]);
            textBox95.Text = TextRangeCSV.Name[0];

            // Langue des textes
            for (int i = 0; i < LanguagesCSV.Length; i++)
                textBox89.Items.Add(LanguagesCSV.Name[i]);
            textBox89.Text = LanguagesCSV.Name[0];

            // Ecole de dommage
            for (int i = 0; i < dmgSchoolCSV.Length; i++)
                textBox24.Items.Add(dmgSchoolCSV.Name[i]);
            textBox24.Text = dmgSchoolCSV.Name[0];

            // Classe de l'unité
            for (int i = 0; i < UnitClassCSV.Length; i++)
                textBox29.Items.Add(UnitClassCSV.Name[i]);
            textBox29.Text = UnitClassCSV.Name[0];
            //////////////////////////////////////////////////////////////
        }

        // BOUTON GENERER
        private void button1_Click(object sender, EventArgs e)
        {

            // SI LA PAGE 1 EST ACTIVE
            if (materialTabControl1.SelectedTab == tabPage3)
            {
                textBox83.Text = "INSERT INTO `creature_template` VALUES (" + textBox1.Text + ", " + textBox2.Text + ", " + textBox3.Text + ", " + textBox4.Text + ", " + textBox5.Text + ", " + textBox6.Text + ", " + textBox7.Text + ", " + textBox8.Text + ", " + textBox9.Text + ", " + textBox10.Text + ", '" + textBox11.Text + "', '" + textBox12.Text + "', '" + textBox13.Text + "', " + textBox14.Text + ", " + textBox15.Text + " , " + textBox16.Text + ", " + textBox17.Text + ", " + textBox18.Text + ", " + textBox19.Text + ", " + textBox20.Text + ", " + textBox21.Text + ", " + textBox22.Text + ", " + RankCSV.Value[textBox23.SelectedIndex] + ", " + dmgSchoolCSV.Value[textBox24.SelectedIndex] + ", " + textBox25.Text + ", " + textBox26.Text + ", " + textBox27.Text + ", " + textBox28.Text + ", " + UnitClassCSV.Value[textBox29.SelectedIndex] + ", " + textBox30.Text + ", " + textBox31.Text + ", " + textBox32.Text + ", " + CreatureFamilyCSV.Value[textBox33.SelectedIndex] + ", " + TrainerTypeCSV.Value[textBox34.SelectedIndex] + ", " + textBox35.Text + ", " + ChrClassesCSV.Value[textBox36.SelectedIndex] + ", " + textBox37.Text + ", " + CreatureTypeCSV.Value[textBox38.SelectedIndex] + ", " + textBox39.Text + ", " + textBox40.Text + ", " + textBox41.Text + ", " + textBox42.Text + ", " + textBox43.Text + ", " + textBox44.Text + ", " + textBox45.Text + ", " + textBox46.Text + ", " + textBox47.Text + ", " + textBox48.Text + ", " + textBox49.Text + ", " + textBox50.Text + ", " + textBox51.Text + ", " + textBox52.Text + ", " + textBox53.Text + ", " + textBox54.Text + ", " + textBox55.Text + ", " + textBox56.Text + ", " + textBox57.Text + ", " + textBox58.Text + ", " + textBox59.Text + ", " + textBox60.Text + ", '" + textBox61.Text + "', " + textBox62.Text + ", " + textBox63.Text + ", " + textBox64.Text + ", " + textBox65.Text + ", " + textBox66.Text + ", " + textBox67.Text + ", " + textBox68.Text + ", " + textBox69.Text + ", " + (textBox70.Checked == true ? "1" : "0") + ", " + textBox77.Text + ", " + textBox78.Text + ", " + textBox79.Text + ", " + textBox80.Text + ", '" + textBox81.Text + "', " + textBox82.Text + ");";
            }

            // SI LA PAGE 2 EST ACTIVE
            if (materialTabControl1.SelectedTab == tabPage4)
            {
                // POUR CREATURE_EQUIP_TEMPLATE
                if (textBox84.Text == "" && textBox97.Text != "")
                {
                    textBox83.Text = "INSERT INTO `creature_equip_template` VALUES (" + textBox97.Text + ", " + textBox98.Text + ", " + textBox99.Text + ", " + textBox100.Text + ", " + textBox101.Text + ", " + textBox102.Text + ");";
                }

                // POUR CREATURE_TEXT
                if (textBox84.Text != "" && textBox97.Text == "")
                {
                    textBox83.Text = "INSERT INTO `creature_text` VALUES (" + textBox84.Text + ", " + textBox85.Text + ", " + textBox86.Text + ", '" + textBox87.Text + "', " + TextTypeCSV.Value[textBox88.SelectedIndex] + ", " + LanguagesCSV.Value[textBox89.SelectedIndex] + ", " + textBox90.Text + ", " + textBox91.Text + ", " + textBox92.Text + ", " + textBox93.Text + ", " + textBox94.Text + ", " + TextRangeCSV.Value[textBox95.SelectedIndex] + ", '" + textBox96.Text + "');";
                }

                // POUR CREATURE_TEXT & CREATURE_EQUIP_TEMPLATE
                if (textBox84.Text != "" && textBox97.Text != "")
                {
                    textBox83.Text = "INSERT INTO `creature_text` VALUES (" + textBox84.Text + ", " + textBox85.Text + ", " + textBox86.Text + ", '" + textBox87.Text + "', " + TextTypeCSV.Value[textBox88.SelectedIndex] + ", " + LanguagesCSV.Value[textBox89.SelectedIndex] + ", " + textBox90.Text + ", " + textBox91.Text + ", " + textBox92.Text + ", " + textBox93.Text + ", " + textBox94.Text + ", " + TextRangeCSV.Value[textBox95.SelectedIndex] + ", '" + textBox96.Text + "');" + Environment.NewLine + "INSERT INTO `creature_equip_template` VALUES (" + textBox97.Text + ", " + textBox98.Text + ", " + textBox99.Text + ", " + textBox100.Text + ", " + textBox101.Text + ", " + textBox102.Text + ");";
                }
            }
        }

        // BOUTON INJECTER
        private void button2_Click(object sender, EventArgs e)
        {
            // Lecture du XML pour définire la constante de connexion MySQL -> plus bas

            ArrayList host = XPathNav.SearchXPathNavigator("MySQL", "Hôte");
            ArrayList port = XPathNav.SearchXPathNavigator("MySQL", "Port");
            ArrayList user = XPathNav.SearchXPathNavigator("MySQL", "ID");
            ArrayList pass = XPathNav.SearchXPathNavigator("MySQL", "MDP");
            ArrayList db = XPathNav.SearchXPathNavigator("MySQL", "DB");

            // Conversion du ArrayList en String
            StringBuilder hostto = new StringBuilder();
            foreach (object obj in host) hostto.Append(obj);
            string newhost = hostto.ToString();

            // Conversion du ArrayList en String
            StringBuilder portto = new StringBuilder();
            foreach (object obj in port) portto.Append(obj);
            string newport = portto.ToString();

            // Conversion du ArrayList en String
            StringBuilder userto = new StringBuilder();
            foreach (object obj in user) userto.Append(obj);
            string newuser = userto.ToString();

            // Conversion du ArrayList en String
            StringBuilder passto = new StringBuilder();
            foreach (object obj in pass) passto.Append(obj);
            string newpass = passto.ToString();

            // Conversion du ArrayList en String
            StringBuilder dbto = new StringBuilder();
            foreach (object obj in db) dbto.Append(obj);
            string newdb = dbto.ToString();


            // SI LA PAGE 1 EST ACTIVE
            if (materialTabControl1.SelectedTab == tabPage3)
            {

                // Définition de la constante de connexion MySQL
                String constring = @"server=" + newhost + ";port=" + newport + ";database=" + newdb + ";userid=" + newuser + ";password=" + newpass + ";";
                MySqlConnection conDataBase = new MySqlConnection(constring);

                // Définition de la requête SQL
                String cmdText = "INSERT INTO `creature_template` VALUES (" + textBox1.Text + ", " + textBox2.Text + ", " + textBox3.Text + ", " + textBox4.Text + ", " + textBox5.Text + ", " + textBox6.Text + ", " + textBox7.Text + ", " + textBox8.Text + ", " + textBox9.Text + ", " + textBox10.Text + ", '" + textBox11.Text + "', '" + textBox12.Text + "', '" + textBox13.Text + "', " + textBox14.Text + ", " + textBox15.Text + ", " + textBox16.Text + ", " + textBox17.Text + ", " + textBox18.Text + ", " + textBox19.Text + ", " + textBox20.Text + ", " + textBox21.Text + ", " + textBox22.Text + ", " + RankCSV.Value[textBox23.SelectedIndex] + ", " + dmgSchoolCSV.Value[textBox24.SelectedIndex] + ", " + textBox25.Text + ", " + textBox26.Text + ", " + textBox27.Text + ", " + textBox28.Text + ", " + UnitClassCSV.Value[textBox29.SelectedIndex] + ", " + textBox30.Text + ", " + textBox31.Text + ", " + textBox32.Text + ", " + CreatureFamilyCSV.Value[textBox33.SelectedIndex] + ", " + TrainerTypeCSV.Value[textBox34.SelectedIndex] + ", " + textBox35.Text + ", " + ChrClassesCSV.Value[textBox36.SelectedIndex] + ", " + textBox37.Text + ", " + CreatureTypeCSV.Value[textBox38.SelectedIndex] + ", " + textBox39.Text + ", " + textBox40.Text + ", " + textBox41.Text + ", " + textBox42.Text + ", " + textBox43.Text + ", " + textBox44.Text + ", " + textBox45.Text + ", " + textBox46.Text + ", " + textBox47.Text + ", " + textBox48.Text + ", " + textBox49.Text + ", " + textBox50.Text + ", " + textBox51.Text + ", " + textBox52.Text + ", " + textBox53.Text + ", " + textBox54.Text + ", " + textBox55.Text + ", " + textBox56.Text + ", " + textBox57.Text + ", " + textBox58.Text + ", " + textBox59.Text + ", " + textBox60.Text + ", '" + textBox61.Text + "', " + textBox62.Text + ", " + textBox63.Text + ", " + textBox64.Text + ", " + textBox65.Text + ", " + textBox66.Text + ", " + textBox67.Text + ", " + textBox68.Text + ", " + textBox69.Text + ", " + (textBox70.Checked == true ? "1" : "0") + ", " + textBox77.Text + ", " + textBox78.Text + ", " + textBox79.Text + ", " + textBox80.Text + ", '" + textBox81.Text + "', " + textBox82.Text + ");";
                MySqlCommand cmd = new MySqlCommand(cmdText, conDataBase);

                try
                {
                    // Injection dans la BDD
                    conDataBase.Open();
                    cmd.Prepare();
                    cmd.ExecuteNonQuery();
                    conDataBase.Close();
                    MessageBox.Show("L'injection a été réalisée avec succès", "Réussi !", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    // S'l y a des erreurs
                    MessageBox.Show(ex.Message);
                }
            }


            // SI LA PAGE 2 EST ACTIVE
            if (materialTabControl1.SelectedTab == tabPage4)
            {

                String constring = @"server=" + newhost + ";port=" + newport + ";database=" + newdb + ";userid=" + newuser + ";password=" + newpass + ";";
                MySqlConnection conDataBase = new MySqlConnection(constring);

                // POUR CREATURE_EQUIP_TEMPLATE
                if (textBox84.Text == "" && textBox97.Text != "")
                {
                    String cmdText = "INSERT INTO `creature_equip_template` VALUES (" + textBox97.Text + ", " + textBox98.Text + ", " + textBox99.Text + ", " + textBox100.Text + ", " + textBox101.Text + ", " + textBox102.Text + ");";
                    MySqlCommand cmd = new MySqlCommand(cmdText, conDataBase);

                    try
                    {
                        conDataBase.Open();
                        cmd.Prepare();
                        cmd.ExecuteNonQuery();
                        conDataBase.Close();
                        MessageBox.Show("L'injection a été réalisée avec succès", "Réussi !", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                // POUR CREATURE_TEXT
                if (textBox84.Text != "" && textBox97.Text == "")
                {
                    String cmdText = "INSERT INTO `creature_text` VALUES (" + textBox84.Text + ", " + textBox85.Text + ", " + textBox86.Text + ", '" + textBox87.Text + "', " + TextTypeCSV.Value[textBox88.SelectedIndex] + ", " + LanguagesCSV.Value[textBox89.SelectedIndex] + ", " + textBox90.Text + ", " + textBox91.Text + ", " + textBox92.Text + ", " + textBox93.Text + ", " + textBox94.Text + ", " + TextRangeCSV.Value[textBox95.SelectedIndex] + ", '" + textBox96.Text + "');";
                    MySqlCommand cmd = new MySqlCommand(cmdText, conDataBase);

                    try
                    {
                        conDataBase.Open();
                        cmd.Prepare();
                        cmd.ExecuteNonQuery();
                        conDataBase.Close();
                        MessageBox.Show("L'injection a été réalisée avec succès", "Réussi !", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                // POUR CREATURE_EQUIP_TEMPLATE & CREATURE_TEXT
                if (textBox84.Text != "" && textBox97.Text != "")
                {
                    String cmdText = "INSERT INTO `creature_text` VALUES (" + textBox84.Text + ", " + textBox85.Text + ", " + textBox86.Text + ", '" + textBox87.Text + "', " + TextTypeCSV.Value[textBox88.SelectedIndex] + ", " + LanguagesCSV.Value[textBox89.SelectedIndex] + ", " + textBox90.Text + ", " + textBox91.Text + ", " + textBox92.Text + ", " + textBox93.Text + ", " + textBox94.Text + ", " + TextRangeCSV.Value[textBox95.SelectedIndex] + ", '" + textBox96.Text + "'); INSERT INTO `creature_equip_template` VALUES (" + textBox97.Text + ", " + textBox98.Text + ", " + textBox99.Text + ", " + textBox100.Text + ", " + textBox101.Text + ", " + textBox102.Text + ");";
                    MySqlCommand cmd = new MySqlCommand(cmdText, conDataBase);

                    try
                    {
                        conDataBase.Open();
                        cmd.Prepare();
                        cmd.ExecuteNonQuery();
                        conDataBase.Close();
                        MessageBox.Show("L'injection a été réalisée avec succès", "Réussi !", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            // Si on quitte le programme avec la "croix"
            DialogResult dr = MessageBox.Show("Voulez-vous vraiment quitter l'application",
                                           "Confirmation",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Warning);

            if (dr == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                Application.Exit();
            }
        }

        private void documentationDeTrinityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // SI LA PAGE 1 EST ACTIVE
            if (materialTabControl1.SelectedTab == tabPage3)
            {
                Process.Start("http://collab.kpsn.org/display/tc/creature_template");
            }

            // SI LA PAGE 2 EST ACTIVE
            if (materialTabControl1.SelectedTab == tabPage4)
            {
                Process.Start("http://collab.kpsn.org/display/tc/creature_text");
                Process.Start("http://collab.kpsn.org/display/tc/creature_equip_template");
            }
        }

        private void documentationDeTrinityWoWEmuFrançaisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // SI LA PAGE 1 EST ACTIVE
            if (materialTabControl1.SelectedTab == tabPage3)
            {
                Process.Start("http://wiki.wow-emu.fr/index.php?title=Bases_de_donn%C3%A9es:world:creature_template");
            }

            // SI LA PAGE 2 EST ACTIVE
            if (materialTabControl1.SelectedTab == tabPage4)
            {
                Process.Start("http://wiki.wow-emu.fr/index.php?title=Bases_de_donn%C3%A9es:world:creature_text");
                Process.Start("http://wiki.wow-emu.fr/index.php?title=Bases_de_donn%C3%A9es:world:creature_equip_template");
            }
        }

        // Parser CSV, ne pas toucher
        public class ReadCSV
        {
            public ReadCSV(string _nameCSV)
            {
                reader = new StreamReader(File.OpenRead(@"CSV/" + _nameCSV));
                _name = new List<string>();
                _value = new List<UInt64>();
                Read();
            }

            public string[] Read()
            {
                while (!reader.EndOfStream)
                {
                    string[] Values = reader.ReadLine().Split(';');
                    Value.Add(UInt64.Parse(Values[0]));
                    Name.Add(Values[1]);
                    Length++;
                }
                return null;
            }

            private List<string> _name;
            private List<UInt64> _value;

            public List<string> Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public List<UInt64> Value
            {
                get { return _value; }
                set { _value = value; }
            }

            public int Length { get; set; }

            private StreamReader reader;
        }

        // Clic sur le bouton immunity_mask
        private void button3_Click_1(object sender, EventArgs e)
        {
            ImmunityMask window = new ImmunityMask();
            window.ShowDialog();
            textBox79.Text = window.getImmunityMask().ToString();
        }

        // Clic sur le bouton npcflag
        private void button4_Click_1(object sender, EventArgs e)
        {
            NPCFlag window = new NPCFlag();
            window.ShowDialog();
            textBox19.Text = window.getNPCFlag().ToString();
        }

        // Clic sur le bouton unit_flag
        private void button5_Click_1(object sender, EventArgs e)
        {
            UnitFlag window = new UnitFlag();
            window.ShowDialog();
            textBox30.Text = window.getUnitFlag().ToString();
        }

        // Clic sur le bouton flag_extra
        private void button6_Click_1(object sender, EventArgs e)
        {
            FlagExtra window = new FlagExtra();
            window.ShowDialog();
            textBox80.Text = window.getFlagExtra().ToString();
        }

        // Clic sur le bouton type_flag
        private void button7_Click_1(object sender, EventArgs e)
        {
            TypeFlag window = new TypeFlag();
            window.ShowDialog();
            textBox39.Text = window.getTypeFlag().ToString();
        }

        // Clic sur le bouton dynamic_flags
        private void button9_Click_1(object sender, EventArgs e)
        {
            DynamicFlag window = new DynamicFlag();
            window.ShowDialog();
            textBox32.Text = window.getDynamicFlag().ToString();
        }

        // Clic sur le bouton unit_flag2
        private void button8_Click_1(object sender, EventArgs e)
        {
            UnitFlag2 window = new UnitFlag2();
            window.ShowDialog();
            textBox31.Text = window.getUnitFlag2().ToString();
        }

        // Clic sur le bouton Recherche de sort
        private void button10_Click_1(object sender, EventArgs e)
        {
            Spell window = new Spell();
            window.ShowDialog();
        }

        // Clic sur le bouton emote
        private void button11_Click_1(object sender, EventArgs e)
        {
            TextEmote window = new TextEmote();
            window.ShowDialog();
            textBox91.Text = window.getEmoteID().ToString();
        }
    }

    // Définition de la Class pour les XML, ne pas toucher
    class XPathNav2
    {
        private static string XMLFilePath2;

        static XPathNav2()
        {
            XMLFilePath2 = @"WE-Editor.xml";
        }

        /// <summary>
        /// Méthode vierge permettant d'activer la classe statique
        /// </summary>

        public static void init() { }

        /// <summary>
        /// Récupère la valeur de l'attribut du noeud recherché dans le fichier de configuration
        /// </summary>
        /// <param name="xPathString">Expression XPath de recherche du noeud</param>
        /// <param name="attribute">Attribut à rechercher</param>
        /// <returns>Une ArrayList contenant la liste des attributs recherchés</returns>

        public static ArrayList SearchXPathNavigator(string xPathString, string attribute)
        {
            // Initilisation des variables
            XPathDocument xpathDoc;
            XPathNavigator xpathNavigator;
            XPathNodeIterator xpathNodeIterator;
            XPathExpression expr;
            ArrayList listOfAttributes = new ArrayList();

            // Parcours du fichier XML
            try
            {
                xpathDoc = new XPathDocument(XMLFilePath2);
                xpathNavigator = xpathDoc.CreateNavigator();

                expr = xpathNavigator.Compile(xPathString);
                xpathNodeIterator = xpathNavigator.Select(expr);

                while (xpathNodeIterator.MoveNext())
                {
                    // On récupère l'attribut
                    listOfAttributes.Add(xpathNodeIterator.Current.GetAttribute(attribute, ""));
                }
            }
            catch (Exception e)
            {
            }
            return listOfAttributes;
        }
    }
}
