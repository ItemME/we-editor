﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/// This file is part of Mihawk - WoW-Emu.

/// WE-Editor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.

/// WE-Editor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.

/// You should have received a copy of the GNU General Public License
/// along with WE-Editor.  If not, see <http://www.gnu.org/licenses/>.

namespace TruiceV2
{
    public partial class ImmunityMask : Form
    {
        public UInt64 m_uiImmunityMask;

        public ImmunityMask()
        {
            InitializeComponent();
        }

        private void immune_mask_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < MainWindow.ImmuneMaskCSV.Length; i++)
                checkedListBox1.Items.Insert(i, MainWindow.ImmuneMaskCSV.Name[i]);
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void immune_mask_FormClosing(object sender, FormClosingEventArgs e)
        {
            for (int i = 0; i < checkedListBox1.CheckedIndices.Count; i++)
            {
                m_uiImmunityMask += MainWindow.ImmuneMaskCSV.Value[checkedListBox1.CheckedIndices[i]];
            }
        }

        public UInt64 getImmunityMask()
        {
            return m_uiImmunityMask;
        } 
    }
}
