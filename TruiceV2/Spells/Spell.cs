﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/// This file is part of Mihawk - WoW-Emu.

/// WE-Editor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.

/// WE-Editor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.

/// You should have received a copy of the GNU General Public License
/// along with WE-Editor.  If not, see <http://www.gnu.org/licenses/>.

namespace TruiceV2
{
    public partial class Spell : Form
    {
        public Spell()
        {
            InitializeComponent();
        }

        // Chargement du fichier CSV et attribution des colonnes avec les données lues.
        private void Spell_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < MainWindow.SpellCSV.Length; i++)
                dataGridView1.Rows.Add(MainWindow.SpellCSV.Value[i], MainWindow.SpellCSV.Name[i]);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        // Bouton Rechercher.
        private void button2_Click(object sender, EventArgs e)
        {
            // Terme à rechercher.
            string searchValue = textBox1.Text;

            // Si c'est un espace ou si le terme est vide, on sort de la fonction.
            if (searchValue == " " || searchValue == "")
                return;

            // On vide la dataGrid et on recherche les sorts avec les termes.
            dataGridView2.Rows.Clear();
            label1.Text = "Résultat de la recherche pour : " + searchValue;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                // On met à jour le dataGrid résultat.
                if (row.Cells[1].Value.ToString().Contains(searchValue))
                    dataGridView2.Rows.Add(row.Cells[0].Value, row.Cells[1].Value);
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Entrée lance la recherche.
            if (e.KeyChar == (char)Keys.Enter)
            {
                button2_Click(sender, (EventArgs)e);
            }
        }
    }
}